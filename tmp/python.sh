#!/bin/sh -e
if [ "$NIX_PROFILES" != "" ]; then
    shell_nix=`echo $0 | sed 's,python\.sh$,shell.nix,'`
    __CMD="python3"
    __ARGS="$@"
    export __CMD
    export __ARGS
    exec nix-shell $shell_nix --keep __CMD --keep __ARGS --pure --run 'export PYTHONPATH="$HOME/.local/lib/python3.11/site-packages:$PYTHONPATH"; $CMD "$ARGS"'
else
    if which python3.10 > /dev/null 2>&1; then
        exec python3.10 "$@"
    else
        exec python "$@"
    fi
fi
