def make_prompt(objective: str) -> str:
    tools_enabled = ""
    tools_prompts = ""
    return f"""
You are an expert task creation AI tasked with creating a list of tasks as a JSON array,
considering the ultimate objective of your team: '''{objective}'''.
Create new tasks based on the objective. Limit tasks types to those that can be completed with the available tools listed below.
Task description should be detailed.
Current tool options are {tools_enabled}.
{tools_prompts}
dependent_task_ids should always be an empty array, or an array of numbers representing the task ID it should pull results from.
{examples}
OBJECTIVE='''{objective}'''
TASK LIST=
"""


agent_info = {
    "system": "You are a task creation AI",
    "make_prompt": make_prompt,
    "arguments": [("objective", "task")],
}
