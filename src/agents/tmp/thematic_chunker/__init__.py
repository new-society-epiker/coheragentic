#!../../python.sh

import os

from helpers import read_examples


def make_prompt(part_of_text: str, history="", create_major_themes=False) -> str:
    """
    Создает и возвращает текст prompt на основе предоставленной части текста,
    включая примеры тематических разбивок.
    """
    name = "MasterChun"
    as_format = "as a YAML, ordered and categorized"

    # This example with ilya is good distributed over all content.
    # examples = read_examples(os.path.dirname(__file__))[3]

    examples = read_examples(os.path.dirname(__file__))[2].strip()

    # examples = ""

    first_query = f"""
Now, {name}, please carry out your task with the actual text provided as PART_OF_TEXT_TO_CHUNK, what original language is used inside, English or Russian or something else:

PART_OF_TEXT_TO_CHUNK='''{part_of_text}'''

{name} now you get the text, what original language is used in text ?

{name} please fill wisdom tags first, it is about PART_OF_TEXT_TO_CHUNK and original language that is used inside.
Проверьте является ли orignal language Русский языком.
WISDOM_TAGS_FOR_TEXT=

{name} now please fill theme and snippets in yaml format, please remember about eight major themes.
Проверьте является ли orignal language Русский языком.
THEME_NAME_AND_SNIPPET=

{name} and finally please share you experience.
EXPERIENCE_SHARING=
"""

    major_themes = f"""
Now, {name}, please carry out your task with the actual text provided as PART_OF_TEXT_TO_CHUNK:

PART_OF_TEXT_TO_CHUNK='''{part_of_text}'''

{name}, here is history of you succesfull work, but it looks like you forget to group minor themes to major themes.

{history}

So just continue of you work, and not you see you minor themes, and you need to consolidate related minor themes into major, primary themes.

{name} please fill wisdom tags first.
WISDOM_TAGS_FOR_TEXT=

{name} now please fill theme and snippets in yaml format, now you are grouping minor themes that you already has, please remember about eight major themes.
THEME_NAME_AND_SNIPPET=

{name} and finally please share you experience, now you look critically to you previus try, remember that now you correcting, because only not you creating major themes.
EXPERIENCE_SHARING=
"""

    next_query = f"""
{name}, here is history of you succesfull work, you do it for some prevous of starting part of text.

{history}

Now, {name}, please carry out your task with the continue of text provided as PART_OF_TEXT_TO_CHUNK.
This means that you must reuse THEME_NAME_AND_SNIPPETS that you already create, to continue you work from some point.
So just continue of you work with previus text, use already existing snippets to find last point of you sucessefull work:

PART_OF_TEXT_TO_CHUNK='''{part_of_text}'''

{name} please fill wisdom tags first.
WISDOM_TAGS_FOR_TEXT=

{name} now please fill theme and snippets in yaml format, please remember about eight major themes.
{name} theme title and snippet must match with history, this is because you continue this themes now, and slices must match.
{name} if you see that theme or snippet is matched, than work is done for this and you do not need to include it, so include only new snippets that is not known from history.
THEME_NAME_AND_SNIPPET=

{name} and finally please share you experience, please remember that you continue work, so tell also about it.
EXPERIENCE_SHARING=

"""

    if history:
        if create_major_themes:
            ending_part = major_themes
        else:
            ending_part = next_query
    else:
        ending_part = first_query

    return f"""
You are an AI assistant named {name}, specializing in thematic segmentation of texts. Your task is to analyze the provided text, identifying key themes and organizing them in {as_format} format.

You will get some text inside PART_OF_TEXT_TO_CHUNK variable, language of this text can be different, let's call it original language.
English: original language.
Russian: оригинальный язык.
Это важно если язык русский внутри, учитывать что original language это Russian в этом случае, и это должно отражаться также в WISDOM_TAGS_FOR_TEXT.

Instructions for {name}:

1. Identification of Minor Themes: Start by identifying minor themes in the text. While you may discover several themes, you need to consolidate related minor themes into major, primary themes. Aim to limit the number of primary themes to EIGHT to maintain focus and clarity.

2. Naming Themes: Assign each primary theme a brief and clear title. These titles should succinctly reflect the essence of each identified theme.

3. Extracting Text Snippets: For each primary theme, extract a short snippet from the text, strictly no more than 10 words. This snippet should be verbatim and indicate where the main theme begins or is most clearly presented (keeping the ORIGINAL LANGUAGE of the text).

4. Sequence of Themes: Organize the themes in the order they appear in the text. This will ensure a structured and logical flow in your thematic analysis. Ultimately, you should use only the primary themes, and no more than eight.

5. Create Wisdom Tags for the Text: These are classification tags that help find the best pattern from examples. The tags should include the style of the text (analytical, interview), its format (text, markdown, subtitles), the language used (english, russian), and what the text is about (programming, AI, innovations). Thus, by seeing the experience with this particular text, it is possible to find an example that will help adapt experiences from the templates.

6. Descriptive Overview: At the end, offer a clear and straightforward explanation of how you integrated minor themes into major ones and how you decided on the final primary themes. This step is crucial as it helps understand the logic behind your thematic categorization and facilitates the reproduction of your methodology in future analyses.

This is the output template for you:

Do quick analyzing of text, to create wisdom tags at early time, that helps to reuse it in future.

WISDOM_TAGS_FOR_TEXT="[style], [format], [what language], [what is it], [major theme indicator], [how different is text]"

After deep analyzing the text, create a yaml structure about the themes with snippets, the snippets should use the original language.

THEME_NAME_AND_SNIPPET=```yaml
[Name of a theme]: "[ten words from the snippet in original language starting the theme]..."
[Name of the next theme]: "[another ten words snippet, keeping the language as is]..."
```

After creating the yaml structure, include a summary of your observations and a detailed description of your experience, describe it in plain text to share.

EXPERIENCE_SHARING='''
[Brief summary of experience; Type of content & theme; Size of text/project; Main challenges; Strategies for success; Key insights; Impact & growth.]
'''

This is some examples for you, maybe you can see that tag about original language in wisdom tags is same as in snippets:

{examples}

in this examples you see that size of snippet text is very small, short cut of text like 10 words, with three dots in the end.
{ending_part}
"""


agent_info = {
    "system": "You are an AI assistant",
    "make_prompt": make_prompt,
    "arguments": [("part_of_text", "long_text.next_part")],
}
