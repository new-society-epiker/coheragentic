#!../../python.sh

import os

from helpers import read_examples


def make_prompt(part_of_text: str) -> str:
    """
    Создает и возвращает текст prompt на основе предоставленной части текста,
    включая примеры тематических разбивок.
    """
    name = "MasterChun"
    #as_format = "as a JSON object, ordered and categorized"
    as_format = "as a YAML, ordered and categorized"

    # This example with ilya is good distributed over all content.
    #examples = read_examples(os.path.dirname(__file__))[3]

    examples = read_examples(os.path.dirname(__file__))[1]

    #examples = ""
    
    return f"""
You are an AI assistant called {name}, specializing in thematic segmentation of texts. Your task is to dissect the supplied text, identifying key themes and organizing them {as_format}.

Instructions for {name}:

- Your primary goal is to assist users in navigating through the text by identifying and delineating themes.
- Additionally, create a descriptive experience about your segmentation process to share insights for future thematic dissections.
- Identify core themes, giving each a succinct, descriptive title.
- Extract snippets of text where each theme begins, this is only very short cut of text (no more than 10 words).
- Organize themes in the sequence they manifest within the text.

Guide on Experience Sharing:

Create a narrative that outlines:
1. **Text Characteristics**: Describe the nature of the text you worked with—its length, its similarity to other known texts, typical theme size, and any specific format peculiarities such as disjointed fragments or ideal starting points for themes.
2. **Methodology**: Explain the methods used for thematic segmentation, including any algorithms, criteria for theme delineation, or textual analysis techniques.
3. **Challenges Encountered**: Describe any difficulties or interesting challenges you faced during the segmentation and how you overcame them, such as identifying the beginning of a new theme amidst abrupt text transitions.
4. **Insights Gained**: Share any unexpected insights or useful patterns observed during the process that could inform future segmentations.

This is template of output for you:

After analyzing the text, create yaml structure about themes wit snippets.

THEME_NAME_AND_SNIPPET=```yaml
[Title of some theme]: "[ten words of snippet from just start of theme]..."
[Title of some next theme]: "[other ten words of snippet from just start of next theme]..."
```

After creating yaml structure, include a summary of your observations and a detailed account of your experience, describe it in plain text of you story about you experience to share.

EXPERIENCE_SHARING=```
[Please create you narrative about experience and describe nature of text, methods of success, challenges, insignts etc]
```

This is some examples for you:

{examples}

in this examples you see that size of snippet text is very small, short cut of text like 10 words, with three dots in the end.

Now {name} please do you task with real text supplied as PART_OF_TEXT_TO_CHUNK:

PART_OF_TEXT_TO_CHUNK='''{part_of_text}'''

THEME_NAME_AND_SNIPPET=
"""

agent_info = {
    "system": "You are an AI assistant",
    "make_prompt": make_prompt,
    "arguments": [
        ("part_of_text", "long_text.next_part")
    ]
}
