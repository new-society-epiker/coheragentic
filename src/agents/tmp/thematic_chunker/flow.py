#!../../python.sh

import os
import sys

import yaml

sys.path.append(os.path.abspath("../.."))

import unittest

# from tools.text_completion import text_completion_tool
from __init__ import agent_info, make_prompt
from utils.dirty_chunker import dirty_chunker_util


def perform_task(text_completion_tool, large_text: str, chunk_size: int = 8192):

    dirty_slices = dirty_chunker_util(large_text, chunk_size)

    idx = 0

    tries = 0
    goodness = 0
    slices_history = ""
    themes_history = ""
    elems_progress = 999
    slices = []
    good = False

    while True:
        print("A", flush=True)
        if goodness - tries < -5:
            # print(prompt)
            # print(compl)
            raise "task failed"

        tries += 1

        if idx == 0 and not good:
            print("B", flush=True)
            prompt = make_prompt(dirty_slices[idx])
        else:
            print("C", flush=True)
            if elems > 8:
                print("D", flush=True)
                elems_progress = elems
                # themes_history += result
                themes_history = valid_result
                prompt = make_prompt(
                    dirty_slices[idx], slices_history + themes_history, True
                )
            else:
                print("E", flush=True)
                elems_progress = 99
                themes_history = ""
                idx += 1
                print("NEXT IDX", flush=True)
                if idx < len(dirty_slices):
                    print("F", flush=True)
                    slices_history += valid_result
                    slices.append(data)
                    prompt = make_prompt(dirty_slices[idx], slices_history)
                else:
                    slices_history += valid_result
                    slices.append(data)
                    print("G", flush=True)
                    return (slices, slices_history)

        print("H", flush=True)
        compl = text_completion_tool(prompt, agent_info["system"])
        print("I", flush=True)

        # шаг 1: находим индекс, где начинается нужный текст
        wtft_index = compl.find("WISDOM_TAGS_FOR_TEXT")
        # шаг 2: делаем срез от этого индекса до конца строки
        if wtft_index != -1:  # проверяем, что подстрока была найдена
            print("J", flush=True)
            result = compl[wtft_index:]
        else:
            # print(prompt)
            # print(compl)
            print("ERROR: не найден маркер WTFT")
            continue

        # Находим начало интересующего нас блока и обрезаем все до '''yaml
        tnas_index = result.find("THEME_NAME_AND_SNIPPET=```yaml")
        if tnas_index == -1:
            # print(prompt)
            # print(compl)
            print("ERROR: TNAS marker not found")
            continue

        # Обрезаем часть перед '''yaml и ищем закрывающие тройные кавычки
        tnas_index += len("THEME_NAME_AND_SNIPPET=```yaml")
        tnas_end_index = result.find("```", tnas_index)
        if tnas_end_index == -1:
            # print(prompt)
            print("OPEN DEBUG============")
            print(compl)
            print("CLOSE DEBUG============")
            print("ERROR: Closing triple quotes not found")
            continue

        print("K", flush=True)
        yaml_content = result[tnas_index:tnas_end_index].strip()

        # Парсим YAML
        try:
            data = yaml.safe_load(yaml_content)
            if isinstance(data, dict):
                print("L", flush=True)
                # print(prompt)
                # print(compl)
                print("Number of elements:", len(data))
                new_elems = len(data)
            else:
                # print(prompt)
                # print(compl)
                print("ERROR: Parsed data is not a dict")
                continue
        except yaml.YAMLError as exc:
            # print(prompt)
            # print(compl)
            print("ERROR: parsing YAML:", exc)
            continue

        print(("IDX NELEM ELEMP", idx, new_elems, elems_progress))
        if new_elems >= elems_progress:
            # print(prompt)
            # print(compl)
            print("ERROR: Major theme grouping is failed")
            continue

        elems = new_elems
        valid_result = result

        print("M", flush=True)

        print(result, flush=True)

        good = True
        goodness += 1
