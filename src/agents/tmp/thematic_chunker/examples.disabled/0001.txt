EXAMPLE_PART_OF_TEXT_TO_CHUNK='''
As technology advances, it evolves faster and faster each year. Nowadays, smartphones and artificial intelligence no longer seem out of the ordinary... Environmental protection is becoming an increasingly relevant topic. Many strive to reduce their carbon footprint by switching to electric vehicles and avoiding plastic...
'''

THEME_NAME_AND_SNIPPET={
  "Technological Innovations": "As technology advances, it evolves faster and faster each year. Nowadays, smartphones and artificial intelligence no longer seem out of the ordinary...",
  "Environmental Concerns": "Environmental protection is becoming an increasingly relevant topic. Many strive to reduce their carbon footprint by switching to electric vehicles and avoiding plastic..."
}
