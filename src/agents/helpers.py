import os


def read_examples(dirname):
    folder_path = os.path.join(dirname, f"examples")
    all_files = os.listdir(folder_path)
    text_files = [file for file in all_files if file.endswith(".txt")]
    all_texts = []
    for file_name in text_files:
        file_path = os.path.join(folder_path, file_name)
        with open(file_path, "r", encoding="utf-8") as file:
            all_texts.append(file.read() + "\n")
    return all_texts
