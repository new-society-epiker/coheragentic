
agent_info = {
    "system": "You are an AI assistant",
    "make_prompt": (lambda x : x),
    "arguments": [("part_of_text", "long_text.next_part")],
}

__all__ = [
    "agent_info",
]

