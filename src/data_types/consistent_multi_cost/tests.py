#!../../shell.sh python

import unittest

from data_type import (
    MAX_COST,
    ConsistentMultiCost,
    DimensionMismatchError,
    InconsistentComparisonError,
)


class TestConsistentMultiCost(unittest.TestCase):
    """Tests for the ConsistentMultiCost class."""

    def test_initialization(self):
        """Ensure that vectors are initialized correctly both with and without given values."""
        cost_vec = ConsistentMultiCost([10, 20, 30])
        self.assertEqual(
            cost_vec.values, [10, 20, 30], "Should initialize with provided values"
        )

        default_vec = ConsistentMultiCost(dim=3)
        self.assertEqual(
            default_vec.values,
            [MAX_COST, MAX_COST, MAX_COST],
            "Should initialize with default MAX_COST values",
        )

    def test_dimension_mismatch(self):
        """Test that errors are raised when vectors of different dimensions are compared."""
        vec1 = ConsistentMultiCost([10, 20])
        vec2 = ConsistentMultiCost([10, 20, 30])
        with self.assertRaises(DimensionMismatchError):
            vec1.ensure_same_dimension(vec2)

    def test_consistent_comparison(self):
        """Ensure that the comparison checks for consistency across dimensions."""
        vec1 = ConsistentMultiCost([5, 5, 5])
        vec2 = ConsistentMultiCost([5, 5, 5])
        self.assertEqual(
            vec1.compare_consistently(vec2), 0, "Identical vectors should be equal"
        )

        vec3 = ConsistentMultiCost([1, 2, 3])
        vec4 = ConsistentMultiCost([3, 2, 1])
        with self.assertRaises(InconsistentComparisonError):
            vec3.compare_consistently(vec4)

    def test_string_representation(self):
        """Check the string representation of cost vectors, especially handling of MAX_COST."""
        vec = ConsistentMultiCost(dim=2)
        self.assertEqual(
            str(vec),
            "ConsistentMultiCost([MAX, MAX])",
            "String representation should replace MAX_COST with 'MaxCost'",
        )


if __name__ == "__main__":
    unittest.main()
