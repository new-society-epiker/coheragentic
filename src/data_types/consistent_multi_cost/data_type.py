from typing import List, Optional

MAX_COST: int = (
    999999999  # Use a high default cost to simulate "infinite" cost effectively.
)


class DimensionMismatchError(Exception):
    """Exception raised when attempting to compare or operate on multi-cost vectors with differing dimensions."""

    pass


class InconsistentComparisonError(Exception):
    """Exception raised when multi-cost vector comparisons are inconsistent across their dimensions."""

    pass


class ConsistentMultiCost:
    def __init__(self, values: Optional[List[int]] = None, dim: Optional[int] = None):
        """
        Initialize a multi-dimensional cost vector with either provided values or default MAX_COST values.
        """
        self.dim = dim or (len(values) if values is not None else 1)
        if values is not None and len(values) != self.dim:
            raise DimensionMismatchError("Value array length must match the dimension.")
        self.values = values[:] if values is not None else [MAX_COST] * self.dim

    def reset(self) -> None:
        """Reset all cost values to MAX_COST for reinitialization."""
        self.values = [MAX_COST] * self.dim

    def ensure_same_dimension(self, rhs) -> None:
        """
        Ensure dimensions match between self and another cost vector;
        otherwise, raise DimensionMismatchError.
        """
        if self.dim != rhs.dim:
            raise DimensionMismatchError(
                "Dimensions must be identical for a meaningful comparison."
            )

    def compare_consistently(self, rhs) -> int:
        """
        Compare two multi-cost vectors with consistent comparison logic across all dimensions;
        raise InconsistentComparisonError if inconsistent.
        """
        consistent_comparison = None
        for a, b in zip(self.values, rhs.values):
            if a != b:
                current_comparison = a < b
                if consistent_comparison is None:
                    consistent_comparison = current_comparison
                elif current_comparison != consistent_comparison:
                    raise InconsistentComparisonError(
                        "Comparison must be consistent across all dimensions (either all less or all greater)."
                    )
        return (
            0 if consistent_comparison is None else (-1 if consistent_comparison else 1)
        )

    def __str__(self) -> str:
        """Provide a string representation of the multi-cost vector, replacing MAX_COST with 'MAX'."""
        return f"ConsistentMultiCost([{', '.join('MAX' if x == MAX_COST else str(x) for x in self.values)}])"

    def __lt__(self, rhs) -> bool:
        self.ensure_same_dimension(rhs)
        return self.compare_consistently(rhs) < 0

    def __le__(self, rhs) -> bool:
        self.ensure_same_dimension(rhs)
        return self.compare_consistently(rhs) <= 0

    def __eq__(self, rhs) -> bool:
        self.ensure_same_dimension(rhs)
        return self.compare_consistently(rhs) == 0

    def __gt__(self, rhs) -> bool:
        self.ensure_same_dimension(rhs)
        return self.compare_consistently(rhs) > 0

    def __ge__(self, rhs) -> bool:
        self.ensure_same_dimension(rhs)
        return self.compare_consistently(rhs) >= 0
