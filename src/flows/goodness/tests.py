#!../../../shell.sh python

import logging
import unittest

from flow import Cargo, Goodness, Improvements

from data_types import ConsistentMultiCost

from flow import NoImprovementError, AttemptLimitExceededError


class LoggingTest(unittest.TestCase):
    def run(self, result=None):
        logging.debug(f"STARTING TEST: {self.id()}")
        super().run(result)


class TestImprovementsMethods(LoggingTest):

    def test_accumulation(self):
        improvements = Improvements(accumulate=True)
        improvements.push(Cargo([100, 0]))
        improvements.push(Cargo([50, 0]))
        self.assertEqual(len(improvements.stack), 2)

    def test_no_accumulation(self):
        improvements = Improvements(accumulate=False)
        improvements.push(Cargo([100, 0]))
        improvements.push(Cargo([50, 0]))
        self.assertEqual(
            len(improvements.stack), 1
        )  # Only the last cargo should be present


class TestGoodness(LoggingTest):

    def test_basic_operations(self):
        goodness = Goodness()
        cargo = Cargo()
        goodness.new_try()
        self.assertRaises(NoImprovementError, goodness.some_success, cargo)

        cargo_low_cost = Cargo([5, 0])
        goodness.new_try()
        goodness.some_success(cargo_low_cost)
        self.assertEqual(len(goodness.goods), 1)

    def test_attempts_limit(self):
        """Tests that an exception is raised when the limit of attempts is exceeded."""

        goodness = Goodness(limit=2)
        cargo = Cargo(cost_value=[10, 0])

        # Performing exactly at the limit should not raise an exception
        goodness.new_try()
        goodness.new_try()

        # The next try should exceed the limit and raise an exception
        with self.assertRaises(AttemptLimitExceededError) as context:
            goodness.new_try()
        self.assertIn("Exceeded the limit of attempts", str(context.exception))

    def test_best_cost_adjustment(self):
        goodness = Goodness()
        cargo = Cargo([10, 0])
        goodness.some_success(cargo)
        self.assertEqual(goodness.best_cost.values, [10, 0])

    def test_sequential_improvements(self):
        goodness = Goodness()
        cargo1 = Cargo([10, 0])
        cargo2 = Cargo([9, 0])
        goodness.some_success(cargo1)
        goodness.some_success(cargo2)
        self.assertEqual(goodness.best_cost.values, [9, 0])

    def test_handling_cost_increase(self):
        goodness = Goodness()
        cargo = Cargo([10, 0])
        goodness.some_success(cargo)
        with self.assertRaises(NoImprovementError):
            goodness.some_success(Cargo([11, 0]))


class TestSimpleCase(LoggingTest):

    def test_without_improvements(self):
        def callback(stage, obj):
            self.assertEqual(stage, 0)
            self.assertIs(obj, goodness)

        goodness = Goodness()
        goodness.simple_case(callback)

    def test_with_improvements_not_good(self):
        def callback(stage, obj):
            self.assertEqual(stage, 1)

        goodness = Goodness(target_cost=ConsistentMultiCost([5, 0]))
        goodness.some_success(Cargo([7, 0]))
        goodness.simple_case(callback)

    def test_with_improvements_good(self):
        def callback(stage, obj):
            self.assertEqual(stage, 2)

        goodness = Goodness(target_cost=ConsistentMultiCost([5, 0]))
        goodness.some_success(Cargo([5, 0]))
        goodness.simple_case(callback)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
