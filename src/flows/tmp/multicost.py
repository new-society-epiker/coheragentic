#!../python.sh

from typing import Dict, List, Optional

MAX_COST: int = 999999999


class MultiCost:
    def __init__(self, value: Optional[List[int]] = None, dim: Optional[int] = None):
        if dim is not None:
            self.dim = dim
        elif value is not None:
            dim = len(value)
            if dim < 1:
                raise Exception("MultiCost: incorrect dimensions")
            self.dim = dim
        else:
            self.dim = 1
        if value is not None:
            if len(value) != self.dim:
                raise Exception("Different dimension")
            self.value = value
        else:
            self.reset()

    def reset(self):
        self.value = [MAX_COST] * self.dim

    def check_dim(self, rhs):
        if self.dim != rhs.dim:
            raise Exception("Different dimension")

    def compare(self, rhs) -> int:

        less = None
        for a, b in zip(self.value, rhs.value):
            if a != b:
                if less is None:
                    less = a < b
                elif less != (a < b):
                    # This is because we do not impact of quality in this case
                    raise Exception(
                        "Comparsion is not known if one dimention is less and different is more"
                    )
        if less is None:
            return 0
        elif less:
            return -1
        else:
            return 1

    def __lt__(self, rhs):
        self.check_dim(rhs)
        if self.dim == 1:
            return self.value[0] < rhs.value[0]
        return self.compare(rhs) < 0

    def __le__(self, rhs):
        self.check_dim(rhs)
        if self.dim == 1:
            return self.value[0] <= rhs.value[0]
        return self.compare(rhs) < 1

    def __eq__(self, rhs):
        self.check_dim(rhs)
        if self.dim == 1:
            return self.value[0] == rhs.value[0]
        return self.compare(rhs) == 0

    def __gt__(self, rhs):
        self.check_dim(rhs)
        if self.dim == 1:
            return self.value[0] > rhs.value[0]
        return self.compare(rhs) > 0

    def __ge__(self, rhs):
        self.check_dim(rhs)
        if self.dim == 1:
            return self.value[0] >= rhs.value[0]
        return self.compare(rhs) > -1
