#!../../shell.sh python

import re
from bisect import bisect_left
from math import ceil, log10
from typing import List

import tiktoken

from text_completion import text_completion_tool

DEBUG_ONLY = True

tenc = tiktoken.encoding_for_model("gpt-4")


def contains_markdown(line):
    # Simple check for common Markdown syntax elements
    return bool(re.search(r"[_*#[\]()]", line))


def return_with_empty_lines(lines):
    restored_lines = []
    for i, line in enumerate(lines):
        restored_lines.append(line)
        if line.strip() and i != len(lines) - 1:
            restored_lines.append("")
    return restored_lines

def is_line_mergable(line, min_words, max_words):
    word_count = len(line.split())
    return not contains_markdown(line) and min_words <= word_count <= max_words

def parse_srt_part(lines: List[str]):
    seq_id = re.match(r"^\d+$", lines[0])
    time_info = re.match(r"^([0-9][0-9]:[0-9][0-9]:[0-9][0-9]),(\d+) --> ([0-9][0-9]:[0-9][0-9]:[0-9][0-9]),(\d+)$", lines[1])
    text = lines[2]
    if seq_id and time_info:
        return (seq_id, time_info.group(1), text)
    return None

def parse_time(line):
    return re.match(r"^@([0-9][0-9]:[0-9][0-9]:[0-9][0-9])@(.*)$", line)

def clean_up_lines(text, min_words=1, max_words=20, window=5, nl_marker="¶"):
    lines = text.strip().split("\n")

    srt_lines = []
    i = 0
    while i < len(lines):
        if i + 3 < len(lines):
            maybe_srt = parse_srt_part(lines[i:i+3])
            if maybe_srt is not None:
                i += 4
                #srt_lines.append("" + maybe_srt[1] + " " + maybe_srt[2])
                #srt_lines.append("[" + maybe_srt[1] + "]")
                srt_lines.append("@" + maybe_srt[1] + "@" + maybe_srt[2])
            else:
                srt_lines.append(lines[i])
                i += 1
        else:
            srt_lines.append(lines[i])
            i += 1

    lines = srt_lines

    clean_lines = []
    committed_clean_lines = []
    i = 0
    counter = 0
    line_passes = False

    while i < len(lines):
        current_line = lines[i].strip()
        if not current_line:
            if i + 1 < len(lines):
                if line_passes and is_line_mergable(lines[i + 1], min_words, max_words):
                    counter += 1
                else:
                    committed_clean_lines.extend(return_with_empty_lines(clean_lines))
                    clean_lines = []
                    committed_clean_lines.append(current_line)
                    counter = 0
        else:
            if not is_line_mergable(current_line, min_words, max_words):
                if DEBUG_ONLY:
                    assert len(clean_lines) == 0
                    assert counter == 0
                line_passes = False
            else:
                line_passes = True

            if counter >= window:
                committed_clean_lines.extend(map(lambda x: x + nl_marker, clean_lines))
                clean_lines = []
            elif counter > 0:
                clean_lines.append(current_line)
            else:
                committed_clean_lines.append(current_line)
        i += 1

    if clean_lines:
        if counter >= window:
            committed_clean_lines.extend(map(lambda x: x + nl_marker, clean_lines))
        else:
            committed_clean_lines.extend(return_with_empty_lines(clean_lines))

    #print(committed_clean_lines)

    return "\n".join(committed_clean_lines)


def custom_format_text(text, width, nl_marker="¶"):
    lines = text.split("\n")
    formatted_text = []
    i = 0
    time = None

    while i < len(lines):
        line = lines[i]
        words = line.split()

        maybe_time = parse_time(line)
        if maybe_time:
            if time is None:
                time = maybe_time.group(1)
                #print(("LINE", line, time))
            lines[i] = maybe_time.group(2)
            line = maybe_time.group(2)

        #print(time)

        if len(line) > width:
            # Split long lines
            padding = ""
            while words:
                new_line = []
                while words and len(" ".join(new_line + [words[0]])) <= width:
                    new_line.append(words.pop(0))
                if words:
                    formatted_text.append(padding + " ".join(new_line))
                else:
                    formatted_text.append(padding + " ".join(new_line) + nl_marker)
                padding = " "
        else:
            next_time = None
            next_line = None
            if i + 1 < len(lines):
                maybe_time = parse_time(lines[i + 1])
                if maybe_time:
                    next_time = maybe_time.group(1)
                    next_line = maybe_time.group(2)
                else:
                    next_line = lines[i + 1]

            # Check possibility to merge with the next line if it exists and both lines are suitable
            if (
                i + 1 < len(lines)
                and len(words) >= 4
                and len(next_line.split()) >= 4
                and len(line) + len(nl_marker) + len(" ".join(next_line.split()))
                <= width
                and not contains_markdown(line)
                and not contains_markdown(next_line)
            ):

                combined_line = line + nl_marker + next_line
                lines[i + 1] = combined_line
            else:
                # If no merging is possible
                if time is not None:
                    formatted_text.append("@" + time + "@" + line + nl_marker)
                    time = None
                else:
                    formatted_text.append(line + nl_marker)
        i += 1

    return "\n".join(formatted_text)


def dirty_chunker_util(
    large_text: str, chunk_size: int = 8192, line_width=120
) -> List[str]:
    large_text = clean_up_lines(large_text)
    large_text = custom_format_text(large_text, line_width)
    lines = large_text.split("\n")

    digits = max(4, ceil(log10(len(lines) + 1)))

    tlen = 0
    tlines = []
    i = 0
    while i < len(lines):
        maybe_time = parse_time(lines[i])
        if maybe_time:
            time = maybe_time.group(1)
            line = f"{i:0{digits}d},{time}¦" + maybe_time.group(2)
        else:
            line = f"{i:0{digits}d}¦" + lines[i]
        ntokens = len(tenc.encode(line))
        tlen += ntokens
        tlines.append((tlen, ntokens, line))
        i += 1

    max_step = int(chunk_size / 2)
    extra = int(max_step / 4)
    if tlen <= chunk_size + extra:
        return [large_text]
    freeshift = tlen - (chunk_size - 2)
    step = int(min(max_step, max(max_step / 4, freeshift / ceil(freeshift / max_step))))

    step_acc = 0
    points = [(0, 0)]
    i = 0
    while i < len(tlines):
        step_acc += tlines[i][1]
        if step_acc >= step:
            step_acc = 0
            points.append((tlines[i][0], i))
        i += 1

    i = 0
    pairs = []
    while i < len(points):
        pos = bisect_left([x[0] for x in tlines], points[i][0] + chunk_size)
        start = points[i][1]
        if pos > start:
            pos -= 1
        else:
            raise ValueError("Some error")
        pairs.append((start, pos))
        size = tlines[pos][0] - points[i][0]
        print(size)
        if pos + 1 >= len(tlines):
            break
        i += 1

    out = []
    for pair in pairs:
        part = "\n".join(map(lambda x: x[2], tlines[pair[0] : pair[1] + 1]))
        out.append(part)

    return out


if __name__ == "__main__":
    # with open("../../ideas/agentic_ways_01.txt", "r", encoding="utf-8") as file:
    #with open("../../ideas/ilya_nvidia_subtitles.txt", "r", encoding="utf-8") as file:
    #with open("../../ideas/versen01.srt", "r", encoding="utf-8") as file:
    with open("../../ideas/agent_wave.srt", "r", encoding="utf-8") as file:
        content = file.read()

    history = ""
    # for a in dirty_chunker_util(content[0:-1600] + content[0:10000] + content[0:101]):
    for a in dirty_chunker_util(content):
        print("\n")
        print("====\n", flush = True)
        #print(a)
        new_data = text_completion_tool("You are youtube subtitles summarizer, please summarize text, line markers can help you to deal with order, also after line number information about youtube time exist, also new line makers '¶' can help you to understand flow. Please provide long and structures summary, and do not forget to use youtube time in summary. please use youtube time ranges in summary. Here is the history: '''{history}''', if history exist than text is like continue for you job. Here is the text: '''" + a + "''', maybe this is not the end of the video, you result will be appended to the history for layer to continue, do the part of you job")
        print(new_data)
        history += new_data
        print("\n====", flush = True)
        print("\n")
