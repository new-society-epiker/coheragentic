def user_input_tool(prompt: str) -> str:
    val = input(f"\n{prompt}\nYour response: ")
    return str(val)


# Метаинформация
tool_info = {
    "name": "user-input",
    "operation": user_input_tool,
    "arguments": [("prompt", "task")],
    "prompt": "Use [user-input] sparingly and only if you need to ask a question to the user who set up the objective. The task description should be the question you want to ask the user.",
}
