import os
import random
import re
import sys
import time
from typing import Dict, List, Optional

import requests
import simplejson
from groq import Groq
from openai import OpenAI

import config


def get_temperature(system):
    if system == "You are an AI assistant":
        return 0.7
    return 0.7


groq_client = None


def get_groq_client():
    global groq_client
    if groq_client == None:
        groq_client = Groq(api_key=config.GROQ_API_KEY)
    return groq_client


def groq_text_completion(prompt: str, system: str) -> str:
    client = get_groq_client()
    response = client.chat.completions.create(
        model="mixtral-8x7b-32768",
        messages=[
            {
                "role": "system",
                "content": system,
            },
            {"role": "user", "content": prompt},
        ],
        # temperature=get_temperature(system),
        # max_tokens=1500,
        # top_p=1,
        # frequency_penalty=0,
        # presence_penalty=0
    )
    return response.choices[0].message.content


openai_client = None


def get_openai_client():
    global openai_client
    if openai_client == None:
        if config.OPENAI_API_ORG:
            openai_client = OpenAI(
                organization=config.OPENAI_API_ORG,
                api_key=config.OPENAI_API_KEY,
            )
        else:
            openai_client = OpenAI(
                api_key=config.OPENAI_API_KEY,
            )
    return openai_client


def openai_text_completion(prompt: str, system: str) -> str:
    client = get_openai_client()
    response = client.chat.completions.create(
        model="gpt-4-turbo",
        # model="gpt-3.5-turbo",
        messages=[
            {
                "role": "system",
                "content": system,
            },
            {"role": "user", "content": prompt},
        ],
        temperature=get_temperature(system),
        # max_tokens=1500,
        # top_p=1,
        # frequency_penalty=0,
        # presence_penalty=0
    )
    return response.choices[0].message.content


def vext_text_completion(prompt: str, key_proj) -> str:
    key = key_proj[0]
    proj = key_proj[1]
    nonce = random.randint(100000000, 999999999)
    chan = f"nonce{nonce}"
    print(("chan", chan))
    url = f"https://payload.vextapp.com/hook/{proj}/catch/{chan}"
    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "Apikey": f"Api-Key {key}",
    }
    data = {"long_polling": "true", "payload": prompt}
    response = requests.post(url, headers=headers, data=simplejson.dumps(data))
    acc = ""
    if response.status_code == 200:
        print(("RESP0", response.text), flush=True)
        request_id = simplejson.loads(response.text)["text"]["request_id"]
        data = {"request_id": request_id}
        while True:
            print(("Sending data", data), flush=True)
            try:
                response = requests.post(
                    url, headers=headers, data=simplejson.dumps(data), timeout=30
                )
            except requests.exceptions.Timeout:
                print("timeout", flush=True)
                continue
            if response.status_code == 202:
                print(("RESP 202", response.text), flush=True)
                time.sleep(0.5)
                continue
            elif response.status_code == 200:
                print(("RESP", response.text), flush=True)
                text = simplejson.loads(response.text)["text"]
                if simplejson.loads(response.text)["request_id"]:
                    request_id = simplejson.loads(response.text)["request_id"]
                    data = {"request_id": request_id}
                    acc += text
                    # continue
                    return acc
                else:
                    return acc
    raise Exception("Status code of some request is not 200 or 202")


def text_completion_tool(
    prompt: str, system="You are an AI assistant", temperature=0.7
) -> str:
    if config.OPENAI_API_KEY:
        return openai_text_completion(prompt, system)
    elif config.GROQ_API_KEY:
        return groq_text_completion(prompt, system)
    elif system == "You are an AI assistant" and config.VEXT_AI_ASSISTANT_API_KEY_PROJ:
        return vext_text_completion(prompt, config.VEXT_AI_ASSISTANT_API_KEY_PROJ)
    elif (
        system == "You are a task creation AI"
        and config.VEXT_TASK_CREATION_AI_API_KEY_PROJ
    ):
        return vext_text_completion(prompt, config.VEXT_TASK_CREATION_AI_API_KEY_PROJ)
    else:
        raise Exception("Не найден API ключ для сервисов")


# Метаинформация
tool_info = {"operation": text_completion_tool, "arguments": [("prompt", "task")]}
