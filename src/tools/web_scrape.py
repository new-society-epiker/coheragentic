import re
from typing import Dict, List, Optional

import config


def fetch_url_content(url: str) -> Optional[str]:
    """
    Функция для получения содержимого веб-страницы по URL.

    Args:
        url (str): URL веб-страницы.

    Returns:
        Optional[str]: Возвращает текст страницы или None в случае ошибки.
    """
    try:
        response = requests.get(url, headers=config.REQUEST_HEADERS)
        response.raise_for_status()
        return response.text
    except requests.exceptions.RequestException as e:
        print(f"Error fetching URL {url}: {e}")
        return None


def tool_operation(url: str) -> Optional[str]:
    """
    Инструмент для веб-скрапинга страницы по заданному URL.

    Args:
        url (str): URL страницы для скрапинга.

    Returns:
        Optional[str]: Извлеченный текст со страницы или None в случае ошибки.
    """
    content = fetch_url_content(url)
    if content is None:
        return None

    soup = BeautifulSoup(content, "html.parser")
    text = soup.get_text(separator=" ", strip=True)
    return text


# Метаинформация
tool_info = {
    "operation": tool_operation,
    "arguments": [
        (
            "url",
            "task_info.url",
        )  # Предполагаем, что url передается в task["task_info"]["url"]
    ],
}
