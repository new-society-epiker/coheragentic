import importlib.util
import os
import re
from typing import Dict, List, Optional

import openai
import requests
from bs4 import BeautifulSoup

import config

TOOLS_DIR = os.path.dirname(__file__)
tools: Dict[str, Dict] = {}


def load_tools():
    global tools
    for filename in os.listdir(TOOLS_DIR):
        if filename.endswith(".py") and filename != "__init__.py":
            module_name = filename[:-3]
            file_path = os.path.join(TOOLS_DIR, filename)
            spec = importlib.util.spec_from_file_location(module_name, file_path)
            module = importlib.util.module_from_spec(spec)
            try:
                spec.loader.exec_module(module)
                if hasattr(module, "tool_info"):
                    tools[module_name] = module.tool_info
            except Exception as e:
                print(
                    f"Не удалось загрузить инструмент {module_name} из-за ошибки: {e}"
                )


load_tools()

print(tools)
