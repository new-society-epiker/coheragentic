#!/bin/sh
find "$@" -regextype "posix-extended" -regex ".*\.(py)" \
  -exec echo 'echo; echo; echo; echo "File: {}"; cat {}' \; \
  | grep -v tmp | sh
