#!/bin/sh -e

if [ "$__NESTED_SH_SCIPT__" = "yes" ]; then
    exit
fi

tmp=`mktemp -p .`

case $1 in
    *)
        __CMD=$1
        shift
        for arg in "$@"
        do
            echo -n $sep >> $tmp
            echo -n $arg >> $tmp
            sep="@ARG@"
        done
        unset sep
        ;;
esac

export __CMD
export __XARGS=`cat $tmp | sed 's,@ARG@,\x00,g' | base64`

#echo ===
#cat $tmp
#echo ===

rm -f $tmp

if [ "$NIX_PROFILES" != "" ]; then
    dir=`echo $0 | sed 's,shell\.sh$,,'`
    shell_nix="$dir/shell.nix"
    export __PROJECT_PYTHONPATH=`realpath $dir`/src
    exec nix-shell $shell_nix \
        --keep __CMD \
        --keep __XARGS \
        --keep __PROJECT_PYTHONPATH \
        --pure --run \
        'export __NESTED_SH_SCIPT__=yes; \
         export PYTHONPATH="$HOME/.local/lib/python3.11/site-packages:$PYTHONPATH"; \
         export PYTHONPATH="$__PROJECT_PYTHONPATH:$PYTHONPATH"; \
         echo "$__XARGS" | base64 -d | xargs -0 $__CMD'
else
    export __NESTED_SH_SCIPT__=yes
    echo "$__XARGS" | base64 -d | xargs -0 $__CMD
fi
