from typing import List, Dict, Optional

# Импорт глобальных настроек
import config

# Список для хранения задач, каждая задача является словарем
task_list: List[Dict] = []

def add_task(task_description: str, tool: str, dependent_task_ids: Optional[List[int]] = None) -> None:
    """
    Функция для добавления новой задачи в список задач.
    
    Args:
        task_description (str): Описание задачи.
        tool (str): Инструмент, необходимый для выполнения задачи.
        dependent_task_ids (Optional[List[int]]): Список ID зависимых задач.
    """
    global task_list

    # Создаем ID для новой задачи как максимальный ID + 1
    task_id = max([task['id'] for task in task_list], default=0) + 1

    # Формирование новой задачи
    new_task = {
        "id": task_id,
        "task": task_description,
        "tool": tool,
        "dependent_task_ids": dependent_task_ids if dependent_task_ids else [],
        "status": "incomplete",
        "output": ""
    }

    # Добавляем задачу в список
    task_list.append(new_task)

def get_task_by_id(task_id: int) -> Dict:
    """
    Функция для получения задачи по её ID.
    
    Args:
        task_id (int): Идентификатор задачи.
    
    Returns:
        Dict: Задача соответствующая заданному ID. Возвращает None, если задача не найдена.
    """
    global task_list

    for task in task_list:
        if task["id"] == task_id:
            return task
    return None

def update_task_status(task_id: int, new_status: str) -> None:
    """
    Функция для обновления статуса задачи.
    
    Args:
        task_id (int): Идентификатор задачи.
        new_status (str): Новый статус задачи.
    """
    global task_list
    
    # Находим задачу и обновляем её статус
    for task in task_list:
        if task["id"] == task_id:
            task["status"] = new_status
            break

def get_incomplete_tasks() -> List[Dict]:
    """
    Функция, возвращающая список невыполненных задач.
    
    Returns:
        List[Dict]: Список невыполненных задач.
    """
    global task_list
    return [task for task in task_list if task["status"] == "incomplete"]


"""
Этот модуль делает базовое управление задачами очень простым и интуитивно понятным. Вот ключевые функции, которые он предоставляет:

- add_task(...): Добавляет новую задачу в список. Это основной способ внесения новых задач в вашу систему.
- get_task_by_id(task_id): Возвращает задачу по её уникальному идентификатору. Очень полезно, когда вам нужно получить детали конкретной задачи для её обработки или отображения.
- update_task_status(task_id, new_status): Обновляет статус выполнения задачи. Эта функция поможет отслеживать прогресс выполнения задач.
- get_incomplete_tasks(): Возвращает список всех невыполненных задач. Это может быть полезно для отображения списка задач, которые необходимо выполнить.
"""
