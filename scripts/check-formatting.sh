#!/bin/sh -e
if [ "$__NESTED_SH_SCIPT__" != "yes" ]; then
  for dir in .. ../..; do if [ -f $dir/scripts/.allow_to_run_nested_scripts ]; then
    $dir/shell.sh sh -e $0; exit $?;
  fi; done; exit 1
fi

set -x

black --check .
isort --check .
